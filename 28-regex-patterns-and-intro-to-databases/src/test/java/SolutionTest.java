import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"6\n" +
					"riya riya@gmail.com\n" +
					"julia julia@julia.me\n" +
					"julia sjulia@gmail.com\n" +
					"julia julia@gmail.com\n" +
					"samantha samantha@gmail.com\n" +
					"tanya tanya@gmail.com"
		);
		setInput(input);

		final String expected = new String(
			"julia\n" +
					"julia\n" +
					"riya\n" +
					"samantha\n" +
					"tanya"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"12\n" +
					"riya riya@gmail.com\n" +
					"julia julia@julia.me\n" +
					"julia sjulia@gmail.com\n" +
					"julia julia@gmail.com\n" +
					"samantha samantha@gmail.com\n" +
					"tanya tanya@gmail.com\n" +
					"riya ariya@gmail.com\n" +
					"julia bjulia@julia.me\n" +
					"julia csjulia@gmail.com\n" +
					"julia djulia@gmail.com\n" +
					"samantha esamantha@gmail.com\n" +
					"tanya ftanya@gmail.com"
		);
		setInput(input);

		final String expected = new String(
			"julia\n" +
					"julia\n" +
					"julia\n" +
					"julia\n" +
					"riya\n" +
					"riya\n" +
					"samantha\n" +
					"samantha\n" +
					"tanya\n" +
					"tanya"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test2() {
		final String input = new String(
			"30\n" +
					"riya riya@gmail.com\n" +
					"julia julia@julia.me\n" +
					"julia sjulia@gmail.com\n" +
					"julia julia@gmail.com\n" +
					"samantha samantha@gmail.com\n" +
					"tanya tanya@gmail.com\n" +
					"riya ariya@gmail.com\n" +
					"julia bjulia@julia.me\n" +
					"julia csjulia@gmail.com\n" +
					"julia djulia@gmail.com\n" +
					"samantha esamantha@gmail.com\n" +
					"tanya ftanya@gmail.com\n" +
					"riya riya@live.com\n" +
					"julia julia@live.com\n" +
					"julia sjulia@live.com\n" +
					"julia julia@live.com\n" +
					"samantha samantha@live.com\n" +
					"tanya tanya@live.com\n" +
					"riya ariya@live.com\n" +
					"julia bjulia@live.com\n" +
					"julia csjulia@live.com\n" +
					"julia djulia@live.com\n" +
					"samantha esamantha@live.com\n" +
					"tanya ftanya@live.com\n" +
					"riya gmail@riya.com\n" +
					"priya priya@gmail.com\n" +
					"preeti preeti@gmail.com\n" +
					"alice alice@alicegmail.com\n" +
					"alice alice@gmail.com\n" +
					"alice gmail.alice@gmail.com"
		);
		setInput(input);

		final String expected = new String(
			"alice\n" +
					"alice\n" +
					"julia\n" +
					"julia\n" +
					"julia\n" +
					"julia\n" +
					"preeti\n" +
					"priya\n" +
					"riya\n" +
					"riya\n" +
					"samantha\n" +
					"samantha\n" +
					"tanya\n" +
					"tanya"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}

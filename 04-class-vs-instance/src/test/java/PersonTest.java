import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class PersonTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"4\n" +
					"-1\n" +
					"10\n" +
					"16\n" +
					"18"
		);
		setInput(input);

		final String expected = new String(
			"Age is not valid, setting age to 0.\n" +
					"You are young.\n" +
					"You are young.\n" +
					"\n" +
					"You are young.\n" +
					"You are a teenager.\n" +
					"\n" +
					"You are a teenager.\n" +
					"You are old.\n" +
					"\n" +
					"You are old.\n" +
					"You are old."
		);

		// run the program with no input arguments
		Person.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}
}

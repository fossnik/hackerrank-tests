import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"1 1 1 0 0 0\n" +
					"0 1 0 0 0 0\n" +
					"1 1 1 0 0 0\n" +
					"0 0 2 4 4 0\n" +
					"0 0 0 2 0 0\n" +
					"0 0 1 2 4 0"
		);
		setInput(input);

		final String expected = new String(
			"19"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test3() {
		final String input = new String(
			"-1 -1 0 -9 -2 -2\n" +
					"-2 -1 -6 -8 -2 -5\n" +
					"-1 -1 -1 -2 -3 -4\n" +
					"-1 -9 -2 -4 -4 -5\n" +
					"-7 -3 -3 -2 -9 -9\n" +
					"-1 -3 -1 -2 -4 -5"
		);
		setInput(input);

		final String expected = new String(
			"-6"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}

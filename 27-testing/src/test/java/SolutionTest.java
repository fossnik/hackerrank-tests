import org.junit.Test;

public class SolutionTest {

	// for this exercise, the tests are already in Solution.java
	@Test
	public void test0() {
		// run the program with no input arguments
		Solution.main(new String[0]);
	}

}

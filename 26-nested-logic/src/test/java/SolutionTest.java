import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"31 8 2004\n" +
					"20 1 2004"
		);
		setInput(input);

		final String expected = new String(
			"3500"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"2 6 2014\n" +
					"5 7 2014"
		);
		setInput(input);

		final String expected = new String(
			"0"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test2() {
		final String input = new String(
			"9 6 2015\n" +
					"6 6 2015"
		);
		setInput(input);

		final String expected = new String(
			"45"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test3() {
		final String input = new String(
			"31 12 2009\n" +
					"1 1 2010"
		);
		setInput(input);

		final String expected = new String(
			"0"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test4() {
		final String input = new String(
			"1 1 2010\n" +
					"31 12 2009"
		);
		setInput(input);

		final String expected = new String(
			"10000"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test5() {
		final String input = new String(
			"1 1 1\n" +
					"8 8 8"
		);
		setInput(input);

		final String expected = new String(
			"0"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test6() {
		final String input = new String(
			"8 4 12\n" +
					"1 1 1"
		);
		setInput(input);

		final String expected = new String(
			"10000"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test7() {
		final String input = new String(
			"23 12 1234\n" +
					"19 9 2468"
		);
		setInput(input);

		final String expected = new String(
			"0"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test8() {
		final String input = new String(
			"24 12 1898\n" +
					"18 9 1898"
		);
		setInput(input);

		final String expected = new String(
			"1500"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test9() {
		final String input = new String(
			"24 10 1776\n" +
					"10 10 1776"
		);
		setInput(input);

		final String expected = new String(
			"210"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}

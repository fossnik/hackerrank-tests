# Day 5: Loops

**Objective**
In this challenge, we're going to use loops to help us do some simple math. Check out the Tutorial tab to
learn more.
**Task**
Given an integer, , print its first multiples. Each multiple (where ) should be printed
on a new line in the form: n x i = result.
**Input Format**
A single integer,.
**Constraints**

**Output Format**
Print lines of output; each line (where ) contains the of in the form:
n x i = result.
**Sample Input**

```
2
```
**Sample Output**

```
2 x 1 = 22 x 2 = 4
2 x 3 = 62 x 4 = 8
2 x 5 = 102 x 6 = 12
2 x 7 = 142 x 8 = 16
2 x 9 = 182 x 10 = 20
```


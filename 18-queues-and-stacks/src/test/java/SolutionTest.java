import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"racecar"
		);
		setInput(input);

		final String expected = new String(
			"The word, racecar, is a palindrome."
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"yes"
		);
		setInput(input);

		final String expected = new String(
			"The word, yes, is not a palindrome."
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}
}

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"7\n" +
					"3\n" +
					"5\n" +
					"2\n" +
					"1\n" +
					"4\n" +
					"6\n" +
					"7"
		);
		setInput(input);

		final String expected = new String(
			"3"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"9\n" +
					"20\n" +
					"50\n" +
					"35\n" +
					"44\n" +
					"9\n" +
					"15\n" +
					"62\n" +
					"11\n" +
					"13"
		);
		setInput(input);

		final String expected = new String(
			"4"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test2() {
		final String input = new String(
			"13\n" +
					"25\n" +
					"39\n" +
					"12\n" +
					"19\n" +
					"9\n" +
					"23\n" +
					"55\n" +
					"31\n" +
					"60\n" +
					"35\n" +
					"41\n" +
					"70\n" +
					"90"
		);
		setInput(input);

		final String expected = new String(
			"5"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}

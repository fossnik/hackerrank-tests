import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"5"
		);
		setInput(input);

		final String expected = new String(
			"1"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"6"
		);
		setInput(input);

		final String expected = new String(
			"2"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test2() {
		final String input = new String(
			"439"
		);
		setInput(input);

		final String expected = new String(
			"3"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test6() {
		final String input = new String(
			"262141"
		);
		setInput(input);

		final String expected = new String(
			"16"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}
}

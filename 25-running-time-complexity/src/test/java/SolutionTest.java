import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"3\n" +
					"12\n" +
					"5\n" +
					"7"
		);
		setInput(input);

		final String expected = new String(
			"Not prime\n" +
					"Prime\n" +
					"Prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"2\n" +
					"31\n" +
					"33"
		);
		setInput(input);

		final String expected = new String(
			"Prime\n" +
					"Not prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test2() {
		final String input = new String(
			"3\n" +
					"89\n" +
					"109\n" +
					"167"
		);
		setInput(input);

		final String expected = new String(
			"Prime\n" +
					"Prime\n" +
					"Prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test3() {
		final String input = new String(
			"2\n" +
					"79\n" +
					"10"
		);
		setInput(input);

		final String expected = new String(
			"Prime\n" +
					"Not prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test4() {
		final String input = new String(
			"2\n" +
					"29\n" +
					"22"
		);
		setInput(input);

		final String expected = new String(
			"Prime\n" +
					"Not prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test5() {
		final String input = new String(
			"1\n" +
					"1"
		);
		setInput(input);

		final String expected = new String(
			"Not prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test6() {
		final String input = new String(
			"1\n" +
					"2"
		);
		setInput(input);

		final String expected = new String(
			"Prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test7() {
		final String input = new String(
			"3\n" +
					"1000000007\n" +
					"100000003\n" +
					"1000003"
		);
		setInput(input);

		final String expected = new String(
			"Prime\n" +
					"Not prime\n" +
					"Prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test8() {
		final String input = new String(
			"10\n" +
					"1000000000\n" +
					"1000000001\n" +
					"1000000002\n" +
					"1000000003\n" +
					"1000000004\n" +
					"1000000005\n" +
					"1000000006\n" +
					"1000000007\n" +
					"1000000008\n" +
					"1000000009"
		);
		setInput(input);

		final String expected = new String(
			"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Prime\n" +
					"Not prime\n" +
					"Prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test9() {
		final String input = new String(
			"30\n" +
					"1\n" +
					"4\n" +
					"9\n" +
					"16\n" +
					"25\n" +
					"36\n" +
					"49\n" +
					"64\n" +
					"81\n" +
					"100\n" +
					"121\n" +
					"144\n" +
					"169\n" +
					"196\n" +
					"225\n" +
					"256\n" +
					"289\n" +
					"324\n" +
					"361\n" +
					"400\n" +
					"441\n" +
					"484\n" +
					"529\n" +
					"576\n" +
					"625\n" +
					"676\n" +
					"729\n" +
					"784\n" +
					"841\n" +
					"907"
		);
		setInput(input);

		final String expected = new String(
			"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Not prime\n" +
					"Prime"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}

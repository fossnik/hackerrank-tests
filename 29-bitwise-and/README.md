# Day 29: Bitwise AND

**Objective**
Welcome to the last day! Today, we're discussing bitwise operations. Check out the Tutorial tab for learning
materials and an instructional video!

**Task**
Given set  . Find two integers, and (where  ), from set such that the
value of is the maximum possible and also less than a given integer,  . In this case, represents the
bitwise AND operator.
**Input Format**
The first line contains an integer,  , the number of test cases. 
Each of the subsequent lines defines a test case as space-separated integers, and  , respectively.
**Constraints**

**Output Format**
For each test case, print the maximum possible value of on a new line.

**Sample Input**

(^3) 5 
8 52 
**Sample Output**
(^14)
0
**Explanation**
All possible values of and are:
1 . 
2 . 
3 . 
4 . 
5 . 


## 6 . 

## 7 . 

## 8 . 

## 9 . 

## 10 . 

The maximum possible value of that is also is  , so we print on a new line.



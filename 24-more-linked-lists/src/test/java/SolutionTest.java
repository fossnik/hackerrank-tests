import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SolutionTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"6\n" +
					"1\n" +
					"2\n" +
					"2\n" +
					"3\n" +
					"3\n" +
					"4"
		);
		setInput(input);

		final String expected = new String(
			"1 2 3 4"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"7\n" +
					"1\n" +
					"1\n" +
					"1\n" +
					"1\n" +
					"1\n" +
					"1\n" +
					"1"
		);
		setInput(input);

		final String expected = new String(
			"1"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test2() {
		final String input = new String(
			"5\n" +
					"2\n" +
					"3\n" +
					"3\n" +
					"4\n" +
					"6"
		);
		setInput(input);

		final String expected = new String(
			"2 3 4 6"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test3() {
		final String input = new String(
			"1\n" +
					"10"
		);
		setInput(input);

		final String expected = new String(
			"10"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test4() {
		final String input = new String(
			"20\n" +
					"3\n" +
					"9\n" +
					"9\n" +
					"11\n" +
					"11\n" +
					"11\n" +
					"11\n" +
					"89\n" +
					"89\n" +
					"100\n" +
					"100\n" +
					"101\n" +
					"102\n" +
					"103\n" +
					"108\n" +
					"200\n" +
					"250\n" +
					"250\n" +
					"250\n" +
					"250"
		);
		setInput(input);

		final String expected = new String(
			"3 9 11 89 100 101 102 103 108 200 250"
		);

		// run the program with no input arguments
		Solution.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}

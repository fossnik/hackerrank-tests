import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class GenericsTest {

	// a stream to record the output of our program
	private ByteArrayOutputStream testOutput;

	// run before each test (prepare for input / output)
	@Before
	public void setUpOutputStream() {
		testOutput = new ByteArrayOutputStream();
		System.setOut(new PrintStream(testOutput));
	}

	private void setInput(String input) {
		System.setIn(
			new ByteArrayInputStream(
				input.getBytes()
			)
		);
	}

	@Test
	public void test0() {
		final String input = new String(
			"3\n" +
					"1\n" +
					"2\n" +
					"3\n" +
					"2\n" +
					"Hello\n" +
					"World"
		);
		setInput(input);

		final String expected = new String(
			"1\n" +
					"2\n" +
					"3\n" +
					"Hello\n" +
					"World"
		);

		// run the program with no input arguments
		Generics.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

	@Test
	public void test1() {
		final String input = new String(
			"7\n" +
					"8\n" +
					"6\n" +
					"7\n" +
					"5\n" +
					"3\n" +
					"0\n" +
					"9\n" +
					"3\n" +
					"Jenny's\n" +
					"Phone\n" +
					"Number"
		);
		setInput(input);

		final String expected = new String(
			"8\n" +
					"6\n" +
					"7\n" +
					"5\n" +
					"3\n" +
					"0\n" +
					"9\n" +
					"Jenny's\n" +
					"Phone\n" +
					"Number"
		);

		// run the program with no input arguments
		Generics.main(new String[0]);

		// get the output
		final String actual = testOutput.toString().trim();

		// test for equivalence
		assertEquals(expected, actual);
	}

}
